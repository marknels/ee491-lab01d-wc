###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01d - wc - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build and test a Word Counting program
###
### @author  @todo yourName <@todo yourMail@hawaii.edu>
### @date    @todo dd_mmm_yyyy
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = wc

all: $(TARGET)

wc: wc.c
	$(CC) $(CFLAGS) -o $(TARGET) wc.c

test: wc
	./wc /etc/passwd

clean:
	rm -f $(TARGET)

