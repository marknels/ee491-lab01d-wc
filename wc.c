/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - wc - EE 491 - Spr 2022
///
/// @file    wc.c
/// @version 1.0 - Initial version
///
/// wc - print newline, word, and byte counts for each file
///
/// @author  @todo yourName <@todo yourMail@hawaii.edu>
/// @date    @todo dd_mmm_yyyy
///
/// @see     https://linuxcommand.org/lc3_man_pages/wc1.html
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

int main() {
	printf("wc\n");

   return 0;
}
